import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddUserComponent } from "./modules/add-user/add-user/add-user.component";
import { AuthGuard } from "./modules/login/auth.guard";
import { LoginComponent } from "./modules/login/login/login.component";
import { UserDetailsComponent } from "./modules/user-details/user-details/user-details.component";
import { UserPageComponent } from "./modules/user-page/user-page/user-page.component";
import { PageNotFoundComponent } from "./shared/modules/page-not-found/page-not-found.component";

const appRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'user-list',
        component: UserPageComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'add-user',
        component: AddUserComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'edit-user/:id',
        component: AddUserComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'user-details/:id',
        component: UserDetailsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
