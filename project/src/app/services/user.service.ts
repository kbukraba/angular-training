import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserCard } from 'src/app/models/user-card';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserDetails } from '../models/user-details';
import { USERS } from '../shared/mock-users';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    public users$: BehaviorSubject<UserDetails[]> = new BehaviorSubject<UserDetails[]>(USERS);

    constructor(private http: HttpClient) { }

    getUserCards(): Observable<UserCard[]> {
        return this.http.get('assets/user-cards.json').pipe(map((data: any) => {
            let userCardsList = data["userCardsList"];
            return userCardsList.map(function (user: any): UserCard {
                return new UserCard(user.id, user.name, user.gender, user.activated);
            });
        }));
    }

    getUsers(id: number) {
        if (!id)
            throw Error('User Id is not valid');

        return this.users$.getValue().find(user => user.id === id);
    }

    //  addUser(user: UserDetails){
    //      this.users$.next(user)
    //  }

    updateUser(user: UserDetails) {
        const index = this.users$.getValue().findIndex(u => u.id === user.id);

        if (!index)
            throw Error('User is not found');

        this.users$.getValue()[index] = user;
    }

}
