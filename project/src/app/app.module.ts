import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/modules/shared.module';
import { AddUserModule } from './modules/add-user/add-user.module';
import { LoginModule } from './modules/login/login.module';
import { UserPageModule } from './modules/user-page/user-page.module';
import { UserDetailsModule } from './modules/user-details/user-details.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        UserPageModule,
        SharedModule,
        AddUserModule,
        LoginModule,
        UserDetailsModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
