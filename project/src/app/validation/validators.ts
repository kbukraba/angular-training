import { ValidatorFn, Validators } from "@angular/forms";


export function requaredValidator(): ValidatorFn {
  return Validators.required;
};
