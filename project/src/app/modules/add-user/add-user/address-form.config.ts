export class AddressFormConfig {

  public country: any;
  public city: any;
  public street: any;
  public post: any;

  constructor(data: any = {}) {
    const {
      country,
      city,
      street,
      post
    } = data;

    this.country = [country];
    this.city = [city];
    this.street = [street];
    this.post = [post];
  }
}
