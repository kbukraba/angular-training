import { FormArray, FormBuilder } from "@angular/forms";
import { UserDetails } from "src/app/models/user-details";
import { requaredValidator } from "src/app/validation/validators";
import { AddressFormConfig } from "./address-form.config";

export class UserFormConfig {

    public name: any;
    public gender: any;
    public addresses: FormArray;

    constructor(fb: FormBuilder, data: UserDetails) {
        this.name = [data.name, requaredValidator()];
        this.gender = [data.gender, requaredValidator()];
        //this.addresses = fb.array([fb.group(new AddressFormConfig(data.address)))]); ????
        this.addresses = fb.array([]);
    };
}
