import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { AddressFormConfig } from './address-form.config';
import { UserFormConfig } from './user-from.config';

@Component({
    selector: 'app-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.css']
})

export class AddUserComponent {

    userForm!: FormGroup;

    constructor(private fb: FormBuilder,
        private sharedService: UserService,
        private route: ActivatedRoute) {

    }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        const user = this.sharedService.getUsers(Number(id));

        if (!user)
            throw Error('User was not found');

        this.userForm = this.fb.group(new UserFormConfig(this.fb, user));
    }

    public onSubmit() {
        console.log(this.userForm);
    }

    get addresses(): FormArray {
        return <FormArray>this.userForm.get('addresses');
    }

    private createAddress(): FormGroup {
        return this.fb.group(new AddressFormConfig());
    }

    public addAddress() {
        this.addresses.push(this.createAddress());
    }

    public removeAddress(index: number) {
        this.addresses.removeAt(index);
    }

    public isUserInfoValid() {
        return this.userForm.get('name')?.valid && this.userForm.get('gender')?.valid;
    }
}
