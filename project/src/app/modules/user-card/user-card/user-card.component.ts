import { Component, OnInit } from '@angular/core';
import { UserDetails } from 'src/app/models/user-details';

import { Input } from '@angular/core';
import { Router } from '@angular/router';
import { UserCard } from 'src/app/models/user-card';

@Component({
    selector: 'app-user-card',
    templateUrl: './user-card.component.html',
    styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {

    @Input() users: UserCard[] = [];

    constructor(private router: Router) { }

    ngOnInit(): void {
    }

    onChangeGender(user: UserCard): void {
        user.gender = !user.gender;
    }

    onHideBoys(): void {
        this.users = this.users.filter(u => u.gender === false);
    }

    onChangeActivation(user: UserCard): void {
        user.activated = !user.activated;
    }

    getButtonStyle(user: UserCard): string {
        return user.activated ? 'success' : '';
    }

    getButtonLabel(user: UserCard): string {
        return user.activated ? 'Activated' : 'Not Activated';
    }

    onEdit(user: UserCard): void {
        this.router.navigate(['/edit-user', user.id]);
    }

    onDetails(user: UserCard): void {
        this.router.navigate(['/user-details', user.id]);
    }
}
