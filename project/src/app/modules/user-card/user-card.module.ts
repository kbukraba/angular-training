import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserCardComponent } from './user-card/user-card.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { GenderPipe } from 'src/app/modules/user-card/pipes/gender.pipe';
import { ActivatedPipe } from './pipes/activated.pipe';


@NgModule({
  declarations: [
    UserCardComponent,
    GenderPipe,
    ActivatedPipe
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    UserCardComponent,
    GenderPipe,
    ActivatedPipe
  ]
})

export class UserCardModule {  }
