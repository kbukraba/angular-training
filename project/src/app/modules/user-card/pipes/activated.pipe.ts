import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'activated'
})
export class ActivatedPipe implements PipeTransform {

  transform(value: boolean, ...args: unknown[]): string {
    return value ? 'Activated' : 'Not Activated';
  }

}
