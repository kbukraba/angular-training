import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserCardModule } from '../user-card/user-card.module';


@NgModule({
  declarations: [
    UserDetailsComponent
  ],
  imports: [
    CommonModule,
    UserCardModule
  ],
  exports:[
      UserDetailsComponent
  ]
})
export class UserDetailsModule { }
