import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserDetails } from 'src/app/models/user-details';
import { UserService } from 'src/app/services/user.service';

@Component({
    selector: 'app-user-details',
    templateUrl: './user-details.component.html',
    styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

    user: UserDetails = {
        id: 0,
        name: '',
        age: 0,
        gender: false,
        activated: false,
        address: {
            country: '',
            city: '',
            street: '',
            post: ''
        }
    };

    constructor(private sharedService: UserService,
        private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        const result = this.sharedService.getUsers(Number(id));

        if (!result)
            throw Error('User was not found');

        this.user = result;
    }

}
