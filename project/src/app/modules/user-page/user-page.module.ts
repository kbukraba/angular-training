import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserPageComponent } from './user-page/user-page.component';
import { SharedModule } from 'src/app/shared/modules/shared.module';
import { UserCardModule } from '../user-card/user-card.module';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
    declarations: [
        UserPageComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        UserCardModule,
        HttpClientModule
    ],
    exports: [
        UserPageComponent
    ]
})
export class UserPageModule { }
