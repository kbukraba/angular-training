import { Component, OnInit } from '@angular/core';
import { UserCard } from 'src/app/models/user-card';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  users: UserCard[] = [];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUserCards().subscribe((data: UserCard[]) => this.users=data);
  }

  onActivatedAll(): void {
    this.users.forEach(u => u.activated = true);
  }
}
