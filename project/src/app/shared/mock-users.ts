import { UserDetails } from '../models/user-details';

export const USERS: UserDetails[] = [
    {
        id: 1,
        name: 'Naruto',
        age: 18,
        gender: true,
        activated: true,
        address: {
            country: 'Fire Contry',
            city: 'Konohagakure',
            street: 'Uzumaki',
            post: '001'
        }
    },
    {
        id: 2,
        name: 'Sasuke',
        age: 19,
        gender: true,
        activated: false,
        address: {
            country: 'Fire Contry',
            city: 'Konohagakure',
            street: 'Uchiha',
            post: '002'
        }
    },
    {
        id: 3,
        name: 'Sakura',
        age: 17,
        gender: false,
        activated: false,
        address: {
            country: 'Fire Contry',
            city: 'Konohagakure',
            street: 'Haruno',
            post: '003'
        }
    }
];
