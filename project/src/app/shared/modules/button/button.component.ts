import { Component, OnInit } from '@angular/core';
import { Input} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input() style : string = "";
  @Input() label : string = "";

  constructor() { }

  ngOnInit(): void {
  }

  getButtonStyle(): string {
    switch (this.style) {
      case "warning": return "btn-warning";
      case "error": return "btn-danger";
      case "success": return "btn-success";
      default: return "btn-secondary";
    }
  }
}
