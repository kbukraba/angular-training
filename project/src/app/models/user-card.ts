export class UserCard {
    constructor(
        public id: number,
        public name: string,
        public gender: boolean,
        public activated: boolean) { }
}
