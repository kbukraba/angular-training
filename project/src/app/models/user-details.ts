import { Address } from "./address";

export interface UserDetails {
    id: number;
    name: string;
    age: number;
    gender: boolean;
    activated: boolean;
    address: Address;
}
